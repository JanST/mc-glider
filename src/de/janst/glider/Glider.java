package de.janst.glider;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Glider extends JavaPlugin {

	public static final String USE_PERMISSION = "glider.use";
	public static final String ITEM_NAME = "�l�aGleiter";
	
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new GliderListener(this), this);
	}
	
	public void onDisable() {
		getServer().getScheduler().cancelTasks(this);
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			return true;
		}
		
		Player player = (Player) sender;
		
		if(label.equalsIgnoreCase("glide")) {
			ItemStack item = new ItemStack(Material.STICK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ITEM_NAME);
			item.setItemMeta(meta);
			player.getInventory().addItem(item);
			player.sendMessage("�aItem gegeben");
			return true;
		}
		
		return false;
		
	}

}
