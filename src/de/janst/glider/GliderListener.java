package de.janst.glider;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class GliderListener implements Listener{

	private JavaPlugin plugin;

	public GliderListener(JavaPlugin plugin) {
		this.plugin = plugin;
	}
	
	private Map<UUID, Long> click = new HashMap<UUID, Long>();
	private Map<UUID, Integer> active = new HashMap<UUID, Integer>();
	
	@EventHandler
	public void onClick(PlayerInteractEvent event) {
		if(event.getPlayer().hasPermission(Glider.USE_PERMISSION)) {
			Player player = event.getPlayer();
			ItemStack item = player.getItemInHand();
			
			if(item == null || !(item.getType() == Material.STICK) || !item.hasItemMeta() || !item.getItemMeta().hasDisplayName())
				return;
			
			if(player.getItemInHand().getItemMeta().getDisplayName().equals(Glider.ITEM_NAME)) {
				if(!isActive(player.getUniqueId())) {
					Location loc = player.getLocation();
					loc.getWorld().playSound(loc, Sound.ENDERDRAGON_WINGS, 1, 1);
					
					player.setVelocity(player.getLocation().getDirection().setY(0.5));
					click.put(player.getUniqueId(), System.currentTimeMillis()+500);
					GlideRunnable gr = new GlideRunnable(player, plugin, this);
					active.put(player.getUniqueId(), gr.getId());
				}
				else 
					click.put(player.getUniqueId(), System.currentTimeMillis()+500);
			}
		}
	}
	
	public boolean isMovingActive(UUID uuid) {
		return click.containsKey(uuid) && click.get(uuid) >= System.currentTimeMillis();
	}
	
	public boolean isActive(UUID uuid) {
		return active.containsKey(uuid);
	}
	
	public void stop(UUID uuid) {
		if(active.containsKey(uuid)) {
			plugin.getServer().getScheduler().cancelTask(active.get(uuid));
			active.remove(uuid);
		}
	}

}
