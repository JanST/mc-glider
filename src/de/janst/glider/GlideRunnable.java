package de.janst.glider;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

public class GlideRunnable implements Runnable{

	private final Player player;
	private final UUID uuid;
	private final int id;
	private final GliderListener gliderListener;
	
	public GlideRunnable(Player player, JavaPlugin plugin, GliderListener gliderListener) {
		this.gliderListener = gliderListener;
		this.player = player;
		this.uuid = player.getUniqueId();
		this.id = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, this, 0, 1);
	}

	public int getId() {
		return id;
	}
	
	@Override
	public void run() {
		if(player.isValid()) {
			if(!player.isOnGround()) {
				Vector v = player.getLocation().getDirection();
				if(gliderListener.isMovingActive(uuid)) {
					v.multiply(0.5);
					v.setY(v.getY()/1.2);
				}
				else {
					v.multiply(0.4);
					v.setY(-0.2);	
				}
				player.setVelocity(v);
			}
			else {
				gliderListener.stop(uuid);
				return;
			}
		}
		else {
			gliderListener.stop(uuid);
			return;
		}
	}
	
	

}
